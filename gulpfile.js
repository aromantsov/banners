var gulp = require('gulp'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    browserSync = require('browser-sync');

gulp.task('sass', function(){
    return gulp.src('resources/assets/sass/modal.scss')
    .pipe(sass({outputStyle: 'expanded'}))
    .pipe(gulp.dest('public/css'))
    .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "/"
        }
    });
});

gulp.task('watch', function(){
    gulp.watch('resources/assets/sass/**/*.scss', gulp.parallel('sass'));
});

gulp.task('default', gulp.parallel('sass', 'browser-sync', 'watch'));