@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">       
             <div class="panel-body">
                <a href="{{ route('sites') }}">Сайты</a>
            </div>
             <div class="panel-body">
                <a href="{{ route('addsite') }}">Добавить сайт</a>
            </div>
            <div class="panel-body">
                <a href="{{ route('places', $place->site_id) }}">Места расположения баннеров</a>
            </div>
            <div class="panel-body">
                <a href="{{ route('banners', $place) }}">Баннера</a>
            </div>
            <div class="panel-body">
                <a href="{{ route('addbanner', $place) }}">Добавить баннер</a>
            </div>

            @if(isset($success))
            <p id="site_success" style="color: blue;">{{ $success }}</p>
            @endif

            <form action="{{ route('updateplace', $place) }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
                <input type="hidden" name="site_id" value="{{ $place->site_id }}">
                <div class="panel-body">
                    <label for="file">Place Description</label>
                    <input type="text" class="form-control" name="description" id="description" placeholder="Place Description" value="{{ $place->description or '' }}" required>
                </div>
                <div class="panel-body">
                    <label for="link">Width</label>
                    <input type="text" class="form-control" name="width" id="width" placeholder="Width" value="{{ $place->width or '' }}">
                </div>
                <div class="panel-body">
                    <input type="submit" name="some_name" class="btn btn-primary" value="save">
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    setTimeout(function(){
        $('#site_success').fadeOut();
    }, 5000);
</script>
@endsection