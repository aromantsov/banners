@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel-body">
                <a href="{{ route('sites') }}">Сайты</a>
            </div>
            <div class="panel-body">
                <a href="{{ route('addsite') }}">Добавить сайт</a>
            </div>
            <div class="panel-body">
                <a href="{{ route('addplace', $site) }}">Добавить место расположения баннеров</a>
            </div>
             <table class="table table-striped">
                <thead>
                    <th>Place</th>
                    <th>Width</th>
                    <th class="text-right">Action</th>
                </thead>
                <tbody>
                    @forelse($places as $place)
                    <tr>
                        <td>{{ $place->description }}</td>
                        <td>{{ $place->width }}</td> 
                        <td class="text-right">
                            <form action="{{ route('removeplace', $place->id) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
                                <input type="hidden" name="_method" value="DELETE"> 
                                {{ csrf_field() }}
                                <a href="{{ route('editplace', $place->id) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                            </form></td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3" class="text-center"><h2>No places</h2></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection