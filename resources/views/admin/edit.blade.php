@extends('layouts.app')

@section('content')
<div class="container">
	<form action="{{ route('updateuser', $user) }}" method="post" class="form-horizontal">
		<input type="hidden" name="_method" value="put">
		{{ csrf_field() }}
		@include('admin.partials.form')
	</form>
</div>
@endsection