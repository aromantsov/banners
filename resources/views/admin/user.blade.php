@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel-body">
                <a href="{{ route('adduser') }}">Добавить пользователя</a>
            </div>
             <div class="panel-body">
                <a href="{{ route('sites') }}">Сайты</a>
            </div>
             <div class="panel-body">
                <a href="{{ route('addsite') }}">Добавить сайт</a>
            </div>

            <table class="table table-striped">
                <thead>
                    <th>Name</th>
                    <th>Email</th>
                    <th class="text-right">Action</th>
                </thead>
                <tbody>
                    @forelse($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td class="text-right">
                            <form action="{{ route('deleteuser', $user) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
                                <input type="hidden" name="_method" value="DELETE"> 
                                {{ csrf_field() }}
                                <a href="{{ route('edituser', $user) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                            </form></td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3" class="text-center"><h2>No banners</h2></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection