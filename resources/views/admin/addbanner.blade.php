@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel-body">
                <a href="{{ route('sites') }}">Сайты</a>
            </div>
             <div class="panel-body">
                <a href="{{ route('addsite') }}">Добавить сайт</a>
            </div>
             <div class="panel-body">
                <a href="{{ route('places', $site) }}">Места расположения баннеров</a>
            </div>
             <div class="panel-body">
                <a href="{{ route('addplace', $site) }}">Добавить место расположения баннеров</a>
            </div>
            <div class="panel-body">
                <a href="{{ route('banners', $place) }}">Баннера</a>
            </div>

            @if(isset($success))
            <p id="banner_success" style="color: blue;">{{ $success }}</p>
            @endif

            <form action="{{ route('upload') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="place" value="{{ $place }}">
                <div class="panel-body">
                    <label for="file">File</label>
                    <input type="file" class="form-control" name="file" id="file" placeholder="title" value="" required>
                </div>
                <div class="panel-body">
                    <label for="link">Link</label>
                    <input type="text" class="form-control" name="link" id="link" placeholder="title" value="">
                </div>
                <div class="panel-body">
                    <input type="submit" name="some_name" class="btn btn-primary" value="save">
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    setTimeout(function(){
        $('#banner_success').fadeOut();
    }, 5000);
</script>
@endsection