@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel-body">
                <a href="{{ route('sites') }}">Сайты</a>
            </div>
             <div class="panel-body">
                <a href="{{ route('addsite') }}">Добавить сайт</a>
            </div>
             <div class="panel-body">
                <a href="{{ route('places', $site->id) }}">Места расположения баннеров</a>
            </div>
             <div class="panel-body">
                <a href="{{ route('addplace', $site->id) }}">Добавить место расположения баннеров</a>
            </div>

            @if(isset($success))
            <p id="site_success" style="color: blue;">{{ $success }}</p>
            @endif

            <form action="{{ route('updatesite', $site) }}" method="post" enctype="multipart/form-data">
            <div><b>Site code: {{ $site->id }}</b></div>
            <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
                <div class="panel-body">
                    <label for="file">Site</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="title" value="{{ $site->name or 'http://' }}" required>
                </div>
<!--                 <div class="panel-body">
                    <label for="link">Width</label>
                    <input type="text" class="form-control" name="width" id="width" placeholder="title" value="{{ $site->width or '' }}">
                </div>
 -->                <div class="panel-body">
                    <input type="submit" name="some_name" class="btn btn-primary" value="save">
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    setTimeout(function(){
        $('#site_success').fadeOut();
    }, 5000);
</script>
@endsection