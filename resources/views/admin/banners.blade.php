@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel-body">
                <a href="{{ route('sites') }}">Сайты</a>
            </div>
             <div class="panel-body">
                <a href="{{ route('addsite') }}">Добавить сайт</a>
            </div>
            <div class="panel-body">
                <a href="{{ route('places', $site) }}">Места расположения баннеров</a>
            </div>
            <div class="panel-body">
                <a href="{{ route('addplace', $site) }}">Добавить место расположения баннеров</a>
            </div>
            <div class="panel-body">
                <a href="{{ route('addbanner', $place) }}">Добавить баннер</a>
            </div>
            <table class="table table-striped">
                <thead>
                    <th>Banner</th>
                    <th>Link</th>
                    <th class="text-right">Action</th>
                </thead>
                <tbody>
                    @forelse($banners as $banner)
                    <tr>
                        <td><img style="width: 100px; height: 50px;" src="{{ asset('/img/' . $banner->image) }}" alt=""></td>
                        <td><a href="{{ $banner->link }}">{{ $banner->link }}</a></td> 
                        <td class="text-right">
                            <form action="{{ route('removebanner', $banner->id) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
                                <input type="hidden" name="_method" value="DELETE"> 
                                {{ csrf_field() }}
                                <a href="{{ route('editbanner', $banner->id) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                            </form></td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3" class="text-center"><h2>No banners</h2></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection