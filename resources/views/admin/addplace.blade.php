@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">       
            <div class="panel-body">
                <a href="{{ route('sites') }}">Сайты</a>
            </div>
             <div class="panel-body">
                <a href="{{ route('addsite') }}">Добавить сайт</a>
            </div>
            <div class="panel-body">
                <a href="{{ route('places', $site) }}">Места расположения баннеров</a>
            </div>
            @if(isset($success))
            <div class="panel-body">
                <a href="{{ route('addbanner', $place_id) }}">Добавить баннер</a>
            </div>
            <p id="site_success" style="color: blue;">{{ $success }}</p>
            @endif
            
            <form action="{{ route('uploadplace') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="site_id" value="{{ $site }}">
                {{ csrf_field() }}
                <div class="panel-body">
                    <label for="file">Place Description</label>
                    <input type="text" class="form-control" name="description" id="description" placeholder="Place Description" value="" required>
                </div>
                <div class="panel-body">
                    <label for="link">Width</label>
                    <input type="text" class="form-control" name="width" id="width" placeholder="Width" value="">
                </div>
                <div class="panel-body">
                    <input type="submit" name="some_name" class="btn btn-primary" value="save">
                </div>
            </form>
        </div>
    </div>
</div>
 @if(isset($success))
<div class="container">
    <div class="row">
        <p>Place code: {{ $place_id }}</p>
        <p>Пожалуйста, вставьте следующий код у себя на сайте в футере как можно ближе к закрывающему тегу body:</p>
        <p>&lt;script&gt;var XHR=("onload" in new XMLHttpRequest())?XMLHttpRequest:XDomainRequest;var xhr=new XHR();xhr.open("GET","https://radio-detali.com.ua/api/{{ $place_id }}",true);xhr.onload=function(){var a=JSON.parse(this.responseText);document.getElementById("outside-banner{{ $place_id }}").innerHTML='&lt;a href="' + a.link + '"&gt;&lt;img src="https://radio-detali.com.ua/img/'+a.image+'" alt=""&gt;&lt;/a&gt;'};xhr.onerror=function(){alert("Error "+this.status)};xhr.send();&lt;/script&gt;</p>
        <p>Пожалуйста, вставьте следующий код в то место на сайте, где будет размещен баннер:</p>
        <p>&lt;div id="outside-banner{{ $place_id }}"&gt;&lt;/div&gt;</p>
    </div>
</div>
@endif

<script>
    setTimeout(function(){
        $('#site_success').fadeOut();
    }, 5000);
</script>
@endsection