@extends('layouts.app')

@section('content')
<div class="container">
	<form action="{{ route('createuser') }}" method="post" class="form-horizontal">
		{{ csrf_field() }}
		@include('admin.partials.form')
	</form>
</div>
@endsection