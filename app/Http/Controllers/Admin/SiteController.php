<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Site;

class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
    	$sites = site::all();
    	return view('admin.sites', ['sites' => $sites]);
    }

    public function add()
    {
    	return view('admin.addsite');
    }

    public function upload(Request $request)
    {
    	$this->validate($request, [
            'name' => 'required|string|url|unique:sites',
        ]);
    	Site::create([
            'name' => $request['name'],
        ]);
        return view('admin.addsite', ['success' => 'success']);
    }

    public function destroy($site)
    {
        Site::destroy($site);
        return redirect()->route('sites');
    }

    public function edit($site)
    {
        $site_data = Site::find($site);
        return view('admin.editsite', ['site' => $site_data]);
    }

    public function update(Request $request, $site)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'url', \Illuminate\Validation\Rule::unique('sites')->ignore($site)],
        ]);

        Site::updateSite($site, $request);

        return redirect()->route('sites');
    }
}
