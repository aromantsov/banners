<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banner;
use App\Place;
use App\SimpleImage;

class BannerController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index($place_id)
    {
        $banners = Banner::getBanners($place_id);
        $site_id = Place::getSiteID($place_id);
        return view('admin.banners', ['banners' => $banners, 'place' => $place_id, 'site' => $site_id]);
    }

    public function add($place_id)
    {
        $site_id = Place::getSiteID($place_id);
        return view('admin.addbanner', ['place' => $place_id, 'site' => $site_id]);
    }

    public function upload(Request $request)
    {
        if(!$request->link){
            $request->link = '#';
        }

        $width = Place::getWidth($request->place);

    	foreach ($request->file() as $f) {
            $new_name = time().'_'.$f->getClientOriginalName();
            $f->move($_SERVER['DOCUMENT_ROOT'] . '/img', $new_name);
            $img = new SimpleImage();
            $new_img = $_SERVER['DOCUMENT_ROOT'] . '/img/' . $new_name;
            $img->load($new_img);
            $img->resizeToWidth($width);
            $img->save_file($new_img);
            Banner::addBanner($request, $new_name);
        }
        $site_id = Place::getSiteID($place_id);
    	return view('admin.addbanner', ['place' => $request->place, 'success' => 'success', 'site' => $site_id]);
    }

    public function destroy($banner)
    {
        $banner_char = Banner::find($banner);
        unlink(public_path('/img/' . $banner_char->image));
        Banner::destroy($banner);
        return redirect()->route('banners', $banner_char->place_id);
    }

    public function edit($banner)
    {
        $banner_data = Banner::find($banner);
        $site_id = Place::getSiteID($place_id);
        return view('admin.editbanner', ['banner' => $banner_data, 'site' => $site_id]);
    }

    public function update(Request $request, $banner)
    {
        if(!$request->link){
            $request->link = '#';
        }

        $width = Place::getWidth($request->place);

        if($request->file()){
            $banner_del = Banner::find($request->id);
            unlink(public_path('/img/' . $banner_del->image));

            foreach ($request->file() as $f) {
                $new_name = time().'_'.$f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . '/img', $new_name);
                $img = new SimpleImage();
                $new_img = $_SERVER['DOCUMENT_ROOT'] . '/img/' . $new_name;
                $img->load($new_img);
                $img->resizeToWidth($width);
                $img->save_file($new_img);
                Banner::updateBannerWithImage($banner, $request, $new_name);
            }
        }else{
            Banner::updateBannerWithoutImage($banner, $request);
        }
        $banner_data = Banner::find($banner);
        $site_id = Place::getSiteID($place_id);
        return view('admin.editbanner', ['banner' => $banner_data, 'success' => 'success', 'site' => $site_id]);
    }
}
