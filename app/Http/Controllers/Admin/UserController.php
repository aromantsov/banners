<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

	public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
    	$users = User::all();
    	return view('admin.user', ['users' => $users]);
    }

    public function add()
    {
    	return view('admin.add');
    }

    public function create(Request $request)
    {
    	$this->validate($request, [
            'name' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password'])
        ]);

        return redirect()->route('users');
    }

    public function edit(User $user)
    {
        return view('admin.edit', ['user' => $user]);
    }

    public function update(Request $request, User $user)
    { 
    	$this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => ['required', 'string', 'email', 'max:255', \Illuminate\Validation\Rule::unique('users')->ignore($user->id)],
            'password' => 'nullable|string|min:6|confirmed',
        ]);

        $user->name = $request['name'];
        $user->email = $request['email'];
        $request['password'] == null ?: $user->password = bcrypt($request['password']);
        $user->save();  

        return redirect()->route('users');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users');
    }

}
