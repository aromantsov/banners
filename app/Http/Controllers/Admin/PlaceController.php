<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Place;

class PlaceController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index($id)
    {
        $places = Place::getPlaces($id);
        return view('admin.places', ['places' => $places, 'site' => $id]);
    }

    public function add($id)
    {
    	return view('admin.addplace', ['site' => $id]);
    }

    public function upload(Request $request)
    {
    	$this->validate($request, [
    		'description' => 'required|string',
    		'width' => 'required|integer|max:900'
    	]);

    	$place_id = Place::addPlace($request);

    	return view('admin.addplace', ['site' => $request['site_id'], 'success' => 'success', 'place_id' => $place_id]);
    }

    public function destroy($place)
    {
    	$res = Place::find($place);
    	Place::destroy($place);
    	return redirect()->route('places', $res->site_id);
    }

    public function edit($place)
    {
    	$place_data = Place::find($place);
        return view('admin.editplace', ['place' => $place_data]);
    }

    public function update(Request $request, $place)
    {
    	$this->validate($request, [
            'description' => 'required|string',
    		'width' => 'required|integer|max:900'
    	]);

    	Place::updatePlace($place, $request);

    	return redirect()->route('places', $request->site_id);
    }
}
