<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;

class ApiController extends Controller
{
    public function index($id)
    {
    	header("Access-Control-Allow-Origin: *");
        $banners = Banner::getBanners($id);
        $count = count($banners);
        $max = $count - 1;
        $selected = rand(0, $max);
        echo json_encode($banners[$selected]);
    }
}
