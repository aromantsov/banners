<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Place extends Model
{
    public static function getPlaces($site)
    {
    	return DB::table('places')->where('site_id', '=', $site)->get();
    }

    public static function addPlace($place)
    {
    	return DB::table('places')->insertGetId(['site_id' => $place['site_id'], 'description' => $place['description'], 'width' => $place['width']]);
    }

    public static function updatePlace($place, $data)
    {
    	DB::table('places')->where('id', '=', $place)->update(['site_id' => $data['site_id'], 'description' => $data['description'], 'width' => $data['width']]);
    }

    public static function getWidth($place)
    {
    	$res = DB::table('places')->where('id', '=', $place)->first();
    	return $res->width;
    }

    public static function getSiteID($place_id)
    {
        $res = DB::table('places')->where('id', '=', $place_id)->first();
        return $res->site_id;
    }
}
