<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Site extends Model
{
    protected $fillable = [
        'name', 'width'
    ];

    public static function updateSite($site, $data)
    {
        DB::table('sites')->where('id', '=', $site)->update(['name' => $data->name, 'updated_at' => date('Y-m-d h:i:m')]);
    }
}
