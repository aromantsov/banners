<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Banner extends Model
{
    public static function addBanner($data, $banner)
    {
        DB::table('banners')->insert(['image' => $banner, 'link' => $data->link, 'place_id' => $data->place]);
    }

    public static function updateBannerWithoutImage($banner, $data)
    {
    	DB::table('banners')->where('id', '=', $banner)->update(['link' => $data->link]);
    }

    public static function updateBannerWithImage($banner, $data, $img)
    {
    	DB::table('banners')->where('id', '=', $banner)->update(['link' => $data->link, 'image' => $img]);
    }

    public static function getBanners($place_id)
    {
        return DB::table('banners')->where('place_id', '=', $place_id)->get();
    }

}
