<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/api/{id}', 'ApiController@index'); 

Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middware'=>['admin']], function(){
	Route::get('/', 'UserController@index')->name('users');
	Route::get('/adduser', 'UserController@add')->name('adduser');
	Route::post('/adduser', 'UserController@create')->name('createuser');
	Route::get('/edituser/{user}', 'UserController@edit')->name('edituser');
	Route::put('/edituser/{user}', 'UserController@update')->name('updateuser');
	Route::delete('/deleteuser/{user}', 'UserController@destroy')->name('deleteuser');

    Route::get('/addbanner/{id}', 'BannerController@add')->name('addbanner');
    Route::post('/addbanner', 'BannerController@upload')->name('upload');

    Route::get('/banners/{id}', 'BannerController@index')->name('banners');
    Route::delete('/banners/{id}', 'BannerController@destroy')->name('removebanner');
    Route::get('/editbanner/{id}', 'BannerController@edit')->name('editbanner');
    Route::put('/editbanner/{id}', 'BannerController@update')->name('update'); 

    Route::get('/sites', 'SiteController@index')->name('sites');
    Route::get('/addsite', 'SiteController@add')->name('addsite');
    Route::post('/addsite', 'SiteController@upload')->name('uploadsite');
    Route::delete('/delsite/{id}', 'SiteController@destroy')->name('removesite');
    Route::get('/editsite/{id}', 'SiteController@edit')->name('editsite');
    Route::put('/updatesite/{id}', 'SiteController@update')->name('updatesite');

    Route::get('/places/{id}', 'PlaceController@index')->name('places');
    Route::get('/addplace/{id}', 'PlaceController@add')->name('addplace');
    Route::post('/addplace', 'PlaceController@upload')->name('uploadplace');
    Route::delete('/removeplace/{id}', 'PlaceController@destroy')->name('removeplace');
    Route::get('/editplace/{id}', 'PlaceController@edit')->name('editplace');
    Route::put('/editplace/{id}', 'PlaceController@update')->name('updateplace');

    // Route::get('/sitebanners/{id}', 'SitebannerController@index')->name('sitebanners');
    // Route::get('/addsitebanner/{id}', 'SitebannerController@add')->name('addsitebanner');
    // Route::post('/addsitebanner', 'SitebannerController@upload')->name('uploadsitebanner');
    // Route::delete('/sitebanner{id}', 'SitebannerController@destroy')->name('removesitebanner');
    // Route::get('/editsitebanner/{id}', 'BannerController@edit')->name('editsitebanner');
    // Route::put('/editsitebanner/{id}', 'BannerController@update')->name('updatesitebanner');

});

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
